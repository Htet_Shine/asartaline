<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Township extends Model {

	protected $fillable = ['country_id', 'city_id', 'township_name'];

	public function country() {
		return $this->belongsTo('App\Country', 'country_id');
	}
	public function city() {
		return $this->belongsTo('App\City', 'city_id');
	}
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Food extends Model {

	protected $fillable = ['tastetype_id', 'shop_id', 'name', 'description', 'min_price', 'max_price', 'image', 'suited', 'created_at', 'updated_at'];
	protected $table = 'foods';
	public function taste() {
		return $this->belongsTo('App\TasteType', 'tastetype_id');
	}
	public function shop() {
		return $this->belongsTo('App\Shop', 'shop_id');
	}

}

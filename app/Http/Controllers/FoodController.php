<?php

namespace App\Http\Controllers;

use App\Food;
use App\Shop;
use App\TasteType;
use DB;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Redirect;
use Session;
use View;

class FoodController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		// $foods = DB::table('foods AS f')
		// 	->join('taste_types AS tt', 'f.tastetype_id', '=', 'tt.id')
		// 	->join('shops AS s', 'f.shop_id', '=', 's.id')
		// 	->select('f.*', 'tt.name AS tastetype_name', 's.shop_name AS shop_name')
		// 	->get();
		$foods = Food::with('taste', 'shop')->get();
		return View::make('admin.food.index')
			->with('foods', $foods);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$tastes = TasteType::all();
		$shops = Shop::all();
		return view('admin.food.create', compact('tastes', 'shops'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$this->validate($request, [

			'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

		]);

		$image = $request->file('image');
		$input['imagename'] = time() . '.' . $image->getClientOriginalExtension();
		$destinationPath = public_path('images/food');
		$image->move($destinationPath, $input['imagename']);

		$food = new Food;
		$food->tastetype_id = Input::get('tastetype_id');
		$food->shop_id = Input::get('shop_id');
		$food->name = Input::get('name');
		$food->description = Input::get('description');
		$food->price = Input::get('price');
		$food->image = $input['imagename'];
		$food->suited = Input::get('suited');
		$food->save();
		Session::flash('message', 'Successfully created food!');
		Session::flash('alert-class', 'alert-success alert-dismissible');
		return Redirect::to('admin/food');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		$foods = DB::table('foods AS f')
			->join('shops AS s', 'f.shop_id', '=', 's.id')
			->join('taste_types AS tt', 'f.tastetype_id', '=', 'tt.id')
			->select('f.*', 's.shop_name AS shop_name', 'tt.name AS tastetype_name')
			->where('f.id', '=', $id)
			->first();
		return View::make('admin.food.show')
			->with('foods', $foods);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$foods = Food::findOrFail($id);
		$shops = Shop::all();
		$tastes = TasteType::all();
		return view('admin.food.edit', compact('foods', 'shops', 'tastes'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$food = Food::find($id);
		$food->fill($request->except('image'));

		if ($image = $request->hasFile('image')) {
			$image = $request->file('image');
			$input['imagename'] = time() . '.' . $image->getClientOriginalExtension();
			$destinationPath = public_path('images/food');
			$image->move($destinationPath, $input['imagename']);
			$food->image = $input['imagename'];
		}
		$food->tastetype_id = Input::get('tastetype_id');
		$food->shop_id = Input::get('shop_id');
		$food->name = Input::get('name');
		$food->description = Input::get('description');
		$food->price = Input::get('price');
		$food->suited = Input::get('suited');
		$food->save();

		Session::flash('message', 'Successfully updated food!');
		Session::flash('alert-class', 'alert-success alert-dismissible');
		return Redirect::to('admin/food');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$food = Food::find($id);
		$food->delete();
		Session::flash('message', 'Successfully deleted the food!');
		return Redirect::to('admin/food');
	}
}

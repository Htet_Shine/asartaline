<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Township;
use Illuminate\Http\Request;

class TownshipApiController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$townships = Township::with('country', 'city')
			->get();
		return response()->json($townships);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$township = new Township;
		$township->country_id = $request->input('country_id');
		$township->city_id = $request->input('city_id');
		$township->township_name = $request->input('township_name');

		$township->save();
		return response()->json($township);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		$township = Township::with('country', 'city')
			->where('id', $id)
			->first();
		return response()->json($township);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$township = Township::find($id);
		$township->country_id = $request->input('country_id');
		$township->city_id = $request->input('city_id');
		$township->township_name = $request->input('township_name');
		$township->save();
		return response()->json($township);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$township = Township::find($id);
		if (!$township) {
			return response()->json("Record Not Found", 400);
		}
		$township->delete();
		return response()->json("Successfully Deleted");
	}
}

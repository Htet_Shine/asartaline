<?php

namespace App\Http\Controllers\API;

use App\Food;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FoodApiController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$foods = Food::with('taste', 'shop')->get();
		return response()->json($foods);
	}

	public function search(Request $request) {
		if ($request->input('is_nearby')) {
			if (!$request->input('lat') || !$request->input('lng')) {
				return response()->json("Latitude and Longitude are required.", 400);
			}
			$latitude = $request->input('lat');
			$longitude = $request->input('lng');
			$distance = $request->input('radius') ? $request->input('radius') : 1000; //m
			$location = \DB::select("SELECT shops.id, (((acos(sin(($latitude*pi()/180)) * sin((lat*pi()/180))+cos(($latitude*pi()/180)) * cos((lat*pi()/180)) * cos((($longitude - lng)*pi()/180))))*180/pi())*60*1.1515) AS distance FROM shops  HAVING distance < $distance order By distance asc");
			//dd($location);
			$shop_ids = [];
			$foods = [];
			foreach ($location as $row) {
				$shop_ids[] = $row->id;
			}
			if (count($shop_ids) > 0) {
				if ($request->input('tastetype_id')) {
					$foods = Food::where('name', 'like', '%' . $request->input('name', ''))
						->where('suited', 'like', '%' . $request->input('suited', ''))
						->where('price', '>=', $request->input('min_price', 0))
						->where('price', '<=', $request->input('max_price', 0))
						->where('tastetype_id', $request->input('tastetype_id'))
						->wherein('shop_id', $shop_ids)
						->get();
				} else if ($request->input('shop_id')) {
					$foods = Food::where('name', 'like', '%' . $request->input('name', ''))
						->where('suited', 'like', '%' . $request->input('suited', ''))
						->where('price', '>=', $request->input('min_price', 0))
						->where('price', '<=', $request->input('max_price', 0))
						->where('shop_id', $request->input('shop_id'))
						->wherein('shop_id', $shop_ids)
						->get();
				} else if ($request->input('tastetype_id') && $request->inut('shop_id')) {
					$foods = Food::where('name', 'like', '%' . $request->input('name', ''))
						->where('suited', 'like', '%' . $request->input('suited', ''))
						->where('price', '>=', $request->input('min_price', 0))
						->where('price', '<=', $request->input('max_price', 0))
						->where('tastetype_id', $request->input('tastetype_id', null))
						->where('shop_id', $request->input('shop_id', null))
						->wherein('shop_id', $shop_ids)
						->get();
				} else {
					$foods = Food::where('name', 'like', '%' . $request->input('name', ''))
						->where('suited', 'like', '%' . $request->input('suited', ''))
						->where('price', '>=', $request->input('min_price', 0))
						->where('price', '<=', $request->input('max_price', 9999999999))
						->wherein('shop_id', $shop_ids)
						->get();
				}
			}
			return response()->json($foods);
		} else {
			if ($request->input('tastetype_id')) {
				$foods = Food::where('name', 'like', '%' . $request->input('name', ''))
					->where('suited', 'like', '%' . $request->input('suited', ''))
					->where('price', '>=', $request->input('min_price', 0))
					->where('price', '<=', $request->input('max_price', 9999999))
					->where('tastetype_id', $request->input('tastetype_id'))
					->get();
			} else if ($request->input('shop_id')) {
				$foods = Food::where('name', 'like', '%' . $request->input('name', ''))
					->where('suited', 'like', '%' . $request->input('suited', ''))
					->where('price', '>=', $request->input('min_price', 0))
					->where('price', '<=', $request->input('max_price', 99999999))
					->where('shop_id', $request->input('shop_id'))
					->get();
			} else if ($request->input('tastetype_id') && $request->input('shop_id')) {
				$foods = Food::where('name', 'like', '%' . $request->input('name', ''))
					->where('suited', 'like', '%' . $request->input('suited', ''))
					->where('price', '>=', $request->input('min_price', 0))
					->where('price', '<=', $request->input('max_price', 0))
					->where('tastetype_id', $request->input('tastetype_id', null))
					->where('shop_id', $request->input('shop_id', null))
					->get();
			} else {
				$foods = Food::where('name', 'like', '%' . $request->input('name', ''))
					->where('suited', 'like', '%' . $request->input('suited', ''))
					->where('price', '>=', $request->input('min_price', 0))
					->where('price', '<=', $request->input('max_price', 0))
					->get();
			}
			return response()->json($foods);
		}

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$filename = "";
		if ($request->file('image')) {
			$image = $request->file('image');
			$filename = time() . '.' . $image->getClientOriginalExtension();
			$destinationPath = public_path('images/food');
			$image->move($destinationPath, $filename);
		}

		$food = new Food;
		$food->tastetype_id = $request->input('tastetype_id');
		$food->shop_id = $request->input('shop_id');
		$food->name = $request->input('name');
		$food->description = $request->input('description');
		$food->price = $request->input('price');
		$food->image = $filename;
		$food->suited = $request->input('suited');
		$food->save();
		return response()->json($food);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		$food = Food::with('taste', 'shop')
			->where('id', $id)
			->get();
		return response()->json($food);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$food = Food::find($id);
		$food->fill($request->except('image'));

		if ($image = $request->hasFile('image')) {
			$image = $request->file('image');
			$input['imagename'] = time() . '.' . $image->getClientOriginalExtension();
			$destinationPath = public_path('images/food');
			$image->move($destinationPath, $input['imagename']);
			$food->image = $input['imagename'];
		}
		$food->tastetype_id = $request->input('tastetype_id');
		$food->shop_id = $request->input('shop_id');
		$food->name = $request->input('name');
		$food->description = $request->input('description');
		$food->price = $request->input('price');
		$food->suited = $request->input('suited');
		$food->save();
		return response()->json($food);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$food = Food::find($id);
		if (!$food) {
			return response()->json("Record Not Found", 400);
		}
		$food->delete();
		return response()->json("Successfully Deleted");
	}
}

<?php

namespace App\Http\Controllers\API;

use App\Food;
use App\Http\Controllers\Controller;
use App\TasteType;
use Illuminate\Http\Request;

class TasteTypeApiController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$tastes = TasteType::all();
		return response()->json($tastes);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$taste = new TasteType;
		$taste->name = $request->input('name');

		$taste->save();
		return response()->json($taste);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		$taste = TasteType::find($id);
		return response()->json($taste);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$taste = TasteType::find($id);
		$taste->name = $request->input('name');

		$taste->save();
		return response()->json($taste);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$taste = TasteType::find($id);
		if (!$taste) {
			return response()->json("Record Not Found", 400);
		} else if (Food::where('tastetype_id', '=', $id)->exists()) {
			return response()->json("Cannot Delete this data", 400);
		} else {
			$taste->delete();
			return response()->json("Successfully Deleted");
		}
	}
}

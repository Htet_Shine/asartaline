<?php

namespace App\Http\Controllers\API;

use App\Food;
use App\Http\Controllers\Controller;
use App\Shop;
use Illuminate\Http\Request;

class ShopApiController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$shops = Shop::with('country', 'city', 'township')->get();
		return response()->json($shops);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$shop = new Shop;
		$shop->country_id = $request->input('country_id');
		$shop->city_id = $request->input('city_id');
		$shop->township_id = $request->input('township_id');
		$shop->shop_name = $request->input('shop_name');
		$shop->phone = $request->input('phone');
		$shop->address = $request->input('address');
		$shop->lat = $request->input('lat');
		$shop->lng = $request->input('lng');

		$shop->save();
		return response()->json($shop);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		$shop = Shop::with('country', 'city', 'township')
			->where('id', $id)
			->first();
		return response()->json($shop);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$shop = Shop::find($id);
		$shop->country_id = $request->input('country_id');
		$shop->city_id = $request->input('city_id');
		$shop->township_id = $request->input('township_id');
		$shop->shop_name = $request->input('shop_name');
		$shop->phone = $request->input('phone');
		$shop->address = $request->input('address');
		$shop->lat = $request->input('lat');
		$shop->lng = $request->input('lng');
		$shop->save();
		return response()->json($shop);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$shop = Shop::find($id);
		if (!$shop) {

			return response()->json("Record Not Found", 400);
		} else if (Food::where('shop_id', '=', $id)->exists()) {

			return response()->json("Cannot Delete this data", 400);
		} else {

			$shop->delete();
			return response()->json("Successfully Deleted");
		}
	}
}

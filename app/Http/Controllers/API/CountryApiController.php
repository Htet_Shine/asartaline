<?php

namespace App\Http\Controllers\API;

use App\City;
use App\Country;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CountryApiController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$countries = Country::with('cities')->get();
		return response()->json($countries);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$country = new Country;
		$country->name = $request->input('name');
		$country->code = $request->input('code');

		$country->save();
		return response()->json($country);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		$country = Country::find($id);
		return response()->json($country);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$country = Country::find($id);
		$country->name = $request->input('name');
		$country->code = $request->input('code');

		$country->save();
		return response()->json($country);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$country = Country::find($id);
		if (!$country) {
			return response()->json("Record Not Found", 400);
		} else if (City::where('country_id', '=', $id)->exists()) {
			return response()->json("Cannot Delete this data", 400);
		} else {
			$country->delete();
			return response()->json("Successfully Deleted");
		}
	}
}

<?php

namespace App\Http\Controllers\API;

use App\City;
use App\Http\Controllers\Controller;
use App\Township;
use Illuminate\Http\Request;

class CityApiController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$citis = City::with('country')->get();
		return response()->json($citis);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$city = new City;
		$city->country_id = $request->input('country_id');
		$city->name = $request->input('name');

		$city->save();
		return response()->json($city);

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		$city = City::with('country')
			->where('id', $id)
			->first();
		return response()->json($city);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$city = City::find($id);
		$city->country_id = $request->input('country_id');
		$city->name = $request->input('name');
		$city->save();
		return response()->json($city);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$city = City::find($id);
		if (!$city) {
			return response()->json("Record Not Found", 400);
		} else if (Township::where('city_id', '=', $id)->exists()) {
			return response()->json("Cannot Delete this data", 400);
		} else {
			$city->delete();
			return response()->json("Successfully Deleted");
		}
	}
}

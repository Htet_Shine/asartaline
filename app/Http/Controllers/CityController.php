<?php

namespace App\Http\Controllers;

use App\City;
use App\Country;
use App\Township;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Redirect;
use Session;
use View;

class CityController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {

		// $citis = DB::table('cities AS c')
		// 	->join('countries AS ca', 'c.country_id', '=', 'ca.id')
		// 	->select('c.*', 'ca.name AS country_name')
		// 	->get();
		$citis = City::with('country')->get();
		return View::make('admin.city.index')
			->with('citis', $citis);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$cous = Country::pluck('name', 'id');
		return View::make('admin.city.create')
			->with('cous', $cous);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		// validate
		$cit = new City;
		$cit->country_id = Input::get('country_name');
		$cit->name = Input::get('name');
		$cit->save();
		// redirect
		Session::flash('message', 'Successfully created city!');
		Session::flash('alert-class', 'alert-success alert-dismissible');
		return Redirect::to('admin/city');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		$citis = DB::table('cities AS c')
			->join('countries AS ca', 'c.country_id', '=', 'ca.id')
			->select('c.*', 'ca.name AS country_name')
			->where('c.id', '=', $id)
			->first();
		return View::make('admin.city.show')
			->with('citis', $citis);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$city = City::findOrFail($id);
		$cou = Country::all();
		return view('admin.city.edit', compact('cou', 'city'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$city = City::find($id);
		$city->country_id = Input::get('country_id');
		$city->name = Input::get('name');
		$city->save();
		Session::flash('message', 'Successfully updated city!');
		Session::flash('alert-class', 'alert-success alert-dismissible');
		return Redirect::to('admin/city');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		if (Township::where('city_id', '=', $id)->exists()) {

			Session::flash('message', 'You cannot delete city! because it is used in other list');
			Session::flash('alert-class', 'alert-danger alert-dismissible');
			return Redirect::to('admin/country');
		} else {
			$city = City::find($id);
			$city->delete();
			Session::flash('message', 'Successfully deleted the city!');
			return Redirect::to('admin/city');
		}
	}
}

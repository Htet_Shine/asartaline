<?php

namespace App\Http\Controllers;

use App\City;
use App\Country;
use App\Food;
use App\Shop;
use App\Township;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Redirect;
use Session;
use View;

class ShopController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		// $shops = DB::table('shops AS s')
		// 	->join('countries AS co', 's.country_id', '=', 'co.id')
		// 	->join('cities AS c', 's.city_id', '=', 'c.id')
		// 	->join('townships AS t', 's.township_id', '=', 't.id')
		// 	->select('s.*', 'co.name AS country_name', 'c.name AS city_name', 't.township_name AS township_name')
		// 	->get();
		$shops = Shop::with('country', 'city', 'township')->get();
		return View::make('admin.shop.index')
			->with('shops', $shops);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$cou = Country::all();
		$city = City::all();
		$towns = Township::all();
		return view('admin.shop.create', compact('cou', 'city', 'towns'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		// validate
		$rules = array(
			'country_id' => 'required',
			'city_id' => 'required',
			'township_id' => 'required',
			'shop_name' => 'required',
			'phone' => 'required',
			'address' => 'required',
			'lat' => 'required',
			'lng' => 'required',
		);
		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails()) {
			return Redirect::to('admin/shop/create')
				->withErrors($validator);
		} else {
			// store
			$shop = new Shop;
			$shop->country_id = Input::get('country_id');
			$shop->city_id = Input::get('city_id');
			$shop->township_id = Input::get('township_id');
			$shop->shop_name = Input::get('shop_name');
			$shop->phone = Input::get('phone');
			$shop->address = Input::get('address');
			$shop->lat = Input::get('lat');
			$shop->lng = Input::get('lng');

			$shop->save();

			// redirect
			Session::flash('message', 'Successfully created shop!');
			Session::flash('alert-class', 'alert-success alert-dismissible');
			return Redirect::to('admin/shop');
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		$shops = DB::table('shops AS s')
			->join('countries AS ca', 's.country_id', '=', 'ca.id')
			->join('cities AS c', 's.city_id', '=', 'c.id')
			->join('townships AS t', 's.township_id', '=', 't.id')
			->select('s.*', 'ca.name AS country_name', 'c.name AS city_name', 't.township_name AS township_name')
			->where('s.id', '=', $id)
			->first();
		return View::make('admin.shop.show')
			->with('shops', $shops);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$shops = Shop::findOrFail($id);
		$cou = Country::all();
		$city = City::all();
		$towns = Township::all();
		return view('admin.shop.edit', compact('towns', 'cou', 'city', 'shops'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$shops = Shop::find($id);
		$shops->country_id = Input::get('country_id');
		$shops->city_id = Input::get('city_id');
		$shops->township_id = Input::get('township_id');
		$shops->shop_name = Input::get('shop_name');
		$shops->phone = Input::get('phone');
		$shops->address = Input::get('address');
		$shops->lat = Input::get('lat');
		$shops->lng = Input::get('lng');
		$shops->save();
		Session::flash('message', 'Successfully updated shop!');
		Session::flash('alert-class', 'alert-success alert-dismissible');
		return Redirect::to('admin/shop');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		dd($id);
		if (Food::where('shop_id', '=', $id)->exists()) {

			Session::flash('message', 'You cannot delete shop! because it is used in other list');
			Session::flash('alert-class', 'alert-danger alert-dismissible');
			return Redirect::to('admin/shop');
		} else {
			$shops = Shop::find($id);
			$shops->delete();
			Session::flash('message', 'Successfully deleted the shop!');
			return Redirect::to('admin/shop');
		}
	}

	public function findProductName(Request $request) {
		//if our chosen id and products table prod_cat_id col match the get first 100 data
		//$request->id here is the id of our chosen option id
		$data = City::select('name', 'id')->where('country_id', $request->id)->take(100)->get();
		return response()->json($data); //then sent this data to ajax success
	}

	public function findProductName1(Request $request) {
		//if our chosen id and products table prod_cat_id col match the get first 100 data

		//$request->id here is the id of our chosen option id
		$data = Township::select('township_name', 'id')->where('city_id', $request->id)->take(100)->get();
		return response()->json($data); //then sent this data to ajax success
	}
}

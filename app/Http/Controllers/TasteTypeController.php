<?php

namespace App\Http\Controllers;

use App\Food;
use App\TasteType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Redirect;
use Session;
use View;

class TasteTypeController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$tastes = TasteType::all();
		return View::make('admin.tastetype.index')
			->with('tastes', $tastes);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		return View::make('admin.tastetype.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		// validate
		$validator = Validator::make($request->all(), [
			'name' => 'required|unique:taste_types',

		]);

		if ($validator->fails()) {
			return redirect('admin/tastetype/create')
				->withErrors($validator)
				->withInput();
		} else {
			// store
			$cat = new TasteType;
			$cat->name = Input::get('name');

			$cat->save();

			// redirect
			Session::flash('message', 'Successfully created taste type!');
			Session::flash('alert-class', 'alert-success alert-dismissible');
			return Redirect::to('admin/tastetype');
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		$taste = TasteType::find($id);
		return View::make('admin.tastetype.show')
			->with('taste', $taste);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$taste = TasteType::find($id);
		return View::make('admin.tastetype.edit')
			->with('taste', $taste);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		// validate
		// read more on validation at http://laravel.com/docs/validation
		$rules = array(
			'name' => 'required',
		);
		$validator = Validator::make(Input::all(), $rules);

		// process the login
		if ($validator->fails()) {
			return Redirect::to('admin/tastetype/' . $id . '/edit')
				->withErrors($validator);
		} else {
			// store
			$taste = TasteType::find($id);
			$taste->name = Input::get('name');

			$taste->save();

			// redirect
			Session::flash('message', 'Successfully updated taste type!');
			Session::flash('alert-class', 'alert-success alert-dismissible');
			return Redirect::to('admin/tastetype');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {

		if (Food::where('tastetype_id', '=', $id)->exists()) {

			Session::flash('message', 'You cannot delete taste type! because it is used in other list');
			Session::flash('alert-class', 'alert-danger alert-dismissible');
			return Redirect::to('admin/tastetype');
		} else {

			// delete
			$taste = TasteType::find($id);
			$taste->delete();

			// redirect
			Session::flash('message', 'Successfully deleted taste type!');
			return Redirect::to('admin/tastetype');
		}
	}
}

<?php

namespace App\Http\Controllers;

use App\City;
use App\Country;
use App\Township;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Redirect;
use Session;
use View;

class TownshipController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		// $townships = DB::table('townships AS t')
		// 	->join('countries AS co', 't.country_id', '=', 'co.id')
		// 	->join('cities AS c', 't.city_id', '=', 'c.id')
		// 	->select('t.*', 'co.name AS country_name', 'c.name AS city_name')
		// 	->get();
		$townships = Township::with('country', 'city')
			->get();
		return View::make('admin.township.index')
			->with('townships', $townships);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$cou = Country::all();
		$city = City::all();
		return view('admin.township.create', compact('cou', 'city'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		// validate
		$rules = array(
			'country_id' => 'required',
			'city_id' => 'required',
			'township_name' => 'required',
		);
		$validator = Validator::make(Input::all(), $rules);

		// process the login
		if ($validator->fails()) {
			return Redirect::to('admin/township/create')
				->withErrors($validator);
		} else {
			// store
			$tow = new Township;
			$tow->country_id = Input::get('country_id');
			$tow->city_id = Input::get('city_id');
			$tow->township_name = Input::get('township_name');

			$tow->save();

			// redirect
			Session::flash('message', 'Successfully created township!');
			Session::flash('alert-class', 'alert-success alert-dismissible');
			return Redirect::to('admin/township');
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		$towns = DB::table('townships AS t')
			->join('countries AS ca', 't.country_id', '=', 'ca.id')
			->join('cities AS c', 't.city_id', '=', 'c.id')
			->select('t.*', 'ca.name AS country_name', 'c.name AS city_name')
			->where('t.id', '=', $id)
			->first();
		return View::make('admin.township.show')
			->with('towns', $towns);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$towns = Township::findOrFail($id);
		$cou = Country::all();
		$city = City::all();
		return view('admin.township.edit', compact('towns', 'cou', 'city'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$towns = Township::find($id);
		$towns->country_id = Input::get('country_id');
		$towns->city_id = Input::get('city_id');
		$towns->township_name = Input::get('township_name');
		$towns->save();
		Session::flash('message', 'Successfully updated township!');
		Session::flash('alert-class', 'alert-success alert-dismissible');
		return Redirect::to('admin/township');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$towns = Township::find($id);
		$towns->delete();
		Session::flash('message', 'Successfully deleted the township!');
		return Redirect::to('admin/township');
	}

	public function findProductName(Request $request) {
		//if our chosen id and products table prod_cat_id col match the get first 100 data
		//$request->id here is the id of our chosen option id
		$data = City::select('name', 'id')->where('country_id', $request->id)->take(100)->get();
		return response()->json($data); //then sent this data to ajax success
	}
}

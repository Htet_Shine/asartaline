<?php

namespace App\Http\Controllers;

use App\City;
use App\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Redirect;
use Session;
use View;

class CountryController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$cous = Country::all();
		return View::make('admin.country.index')
			->with('cous', $cous);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		return View::make('admin.country.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		// validate
		$validator = Validator::make($request->all(), [
			'name' => 'required|unique:countries',
		]);

		if ($validator->fails()) {
			return redirect('admin/country/create')
				->withErrors($validator)
				->withInput();
		} else {
			// store
			$cou = new Country;
			$cou->name = Input::get('name');
			$cou->code = Input::get('code');

			$cou->save();

			// redirect
			Session::flash('message', 'Successfully created country!');
			Session::flash('alert-class', 'alert-success alert-dismissible');
			return Redirect::to('admin/country');
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		$cou = Country::find($id);
		return View::make('admin.country.show')
			->with('cou', $cou);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$cou = Country::find($id);
		return View::make('admin.country.edit')
			->with('cou', $cou);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		// validate
		$rules = array(
			'name' => 'required',
			'code' => 'required',
		);
		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails()) {
			return Redirect::to('admin/country/' . $id . '/edit')
				->withErrors($validator);
		} else {
			// store
			$cou = Country::find($id);
			$cou->name = Input::get('name');
			$cou->code = Input::get('code');

			$cou->save();

			// redirect
			Session::flash('message', 'Successfully updated country!');
			Session::flash('alert-class', 'alert-success alert-dismissible');
			return Redirect::to('admin/country');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		if (City::where('country_id', '=', $id)->exists()) {

			Session::flash('message', 'You cannot delete country! because it is used in other list');
			Session::flash('alert-class', 'alert-danger alert-dismissible');
			return Redirect::to('admin/country');
		} else {
			$cou = Country::find($id);
			$cou->delete();

			// redirect
			Session::flash('message', 'Successfully deleted the country!');
			return Redirect::to('admin/country');
		}

	}
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model {
	protected $fillable = ['country_id', 'city_id', 'township_id', 'shop_name', 'phone', 'address', 'lat', 'lng'];
	public function country() {
		return $this->belongsTo('App\Country', 'country_id');
	}
	public function city() {
		return $this->belongsTo('App\City', 'city_id');
	}
	public function township() {
		return $this->belongsTo('App\Township', 'township_id');
	}
}

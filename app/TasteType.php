<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TasteType extends Model {
	protected $fillable = ['name'];
	function foods() {
		return $this->hasMany('App\Food');
	}
}

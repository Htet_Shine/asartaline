<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('shops', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('country_id')->unsigned()->nullable();
			$table->foreign('country_id')->references('id')->on('countries')->nullable();
			$table->integer('city_id')->unsigned()->nullable();
			$table->foreign('city_id')->references('id')->on('cities')->nullable();
			$table->integer('township_id')->unsigned()->nullable();
			$table->foreign('township_id')->references('id')->on('townships')->nullable();
			$table->string('shop_name')->unique()->nullable();
			$table->string('phone')->nullable();
			$table->string('address')->nullable();
			$table->string('lat')->nullable();
			$table->string('lng')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('shops');
	}
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFoodsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('foods', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('tastetype_id')->unsigned()->nullable();
			$table->foreign('tastetype_id')->references('id')->on('taste_types')->nullable();
			$table->integer('shop_id')->unsigned()->nullable();
			$table->foreign('shop_id')->references('id')->on('shops')->nullable();
			$table->string('name')->unique()->nullable();
			$table->string('description')->nullable();
			$table->string('price')->nullable();
			$table->string('image')->nullable();
			$table->string('suited')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('foods');
	}
}

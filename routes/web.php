<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
	return view('welcome');
});

// Route::get('admin', function () {
// 	return view('admin.adminpen');
// });

Route::get('admin', function () {
	return view('auth.login');
});
Route::get('admin/testpen/test', 'TestController@index');

Route::resource('admin/tastetype', 'TasteTypeController');
Route::resource('admin/country', 'CountryController');
Route::resource('admin/city', 'CityController');
Route::resource('admin/township', 'TownshipController');
Route::get('/findProductName', 'TownshipController@findProductName');

Route::resource('admin/shop', 'ShopController');
Route::get('/findProductName', 'ShopController@findProductName');
Route::get('/findProductName1', 'ShopController@findProductName1');

Route::resource('admin/food', 'FoodController');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

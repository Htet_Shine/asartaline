<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
	return $request->user();
});
Route::group(['middleware' => ['client']], function () {
	Route::resource('city', 'API\CityApiController');
	Route::resource('township', 'API\TownshipApiController');
	Route::resource('tastetype', 'API\TasteTypeApiController');
	Route::resource('country', 'API\CountryApiController');
	Route::resource('shop', 'API\ShopApiController');
	Route::resource('food', 'API\FoodApiController');
	Route::get('searchfoods', 'API\FoodApiController@search');
});

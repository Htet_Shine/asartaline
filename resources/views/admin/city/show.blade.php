
@extends('admin.adminpen')
@section('content1')
<section class="content-header">
      <h1>
                {{ $page_title or "Page Title" }}
                <small>{{ $page_description or null }}</small>
            </h1>
      <ol class="breadcrumb">
        <li><a href="{{ URL::to('/admin/city/create')}}"><i class="fa fa-dashboard"></i> New</a></li>
        <li class="active">Here</li>
      </ol>
    </section>
@endsection
@section('content')
    <div class="row">
        <!-- right column -->
        <div class="col-md-6">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">City Detail</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal">
              <div class="box-body">

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-4 control-label">Country Name : </label>

                  <div class="col-sm-8">
                     <label for="inputPassword3" class="control-label">{{$citis->country_name}}</label>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-4 control-label">City Name : </label>

                  <div class="col-sm-8">
                     <label for="inputPassword3" class="control-label">{{$citis->name}}</label>
                  </div>
                </div>

              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                  <a class="btn btn-default" href="{{ URL::to('admin/city/') }}">Back</a>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
      <!-- /.row -->
@endsection
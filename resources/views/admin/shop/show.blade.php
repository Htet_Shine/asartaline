
@extends('admin.adminpen')
@section('content1')
<section class="content-header">
      <h1>
                {{ $page_title or "Page Title" }}
                <small>{{ $page_description or null }}</small>
            </h1>
      <ol class="breadcrumb">
        <li><a href="{{ URL::to('/admin/shop/create')}}"><i class="fa fa-dashboard"></i> New</a></li>
        <li class="active">Here</li>
      </ol>
    </section>
@endsection
@section('content')
    <div class="row">
        <!-- right column -->
        <div class="col-md-6">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Shop Detail</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal">
              <div class="box-body">

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-4 control-label">Country Name : </label>

                  <div class="col-sm-8">
                     <label for="inputPassword3" class="control-label">{{$shops->country_name}}</label>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-4 control-label">City Name : </label>

                  <div class="col-sm-8">
                     <label for="inputPassword3" class="control-label">{{$shops->city_name}}</label>
                  </div>
                </div>


                 <div class="form-group">
                  <label for="inputPassword3" class="col-sm-4 control-label">Township Name : </label>

                  <div class="col-sm-8">
                     <label for="inputPassword3" class="control-label">{{$shops->township_name}}</label>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-4 control-label">Shop Name : </label>

                  <div class="col-sm-8">
                     <label for="inputPassword3" class="control-label">{{$shops->shop_name}}</label>
                  </div>
                </div>

              </div>

              <div class="form-group">
                  <label for="inputPassword3" class="col-sm-4 control-label">Address : </label>

                  <div class="col-sm-8">
                     <label for="inputPassword3" class="control-label">{{$shops->address}}</label>
                  </div>
                </div>


                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-4 control-label">Phone : </label>

                  <div class="col-sm-8">
                     <label for="inputPassword3" class="control-label">{{$shops->phone}}</label>
                  </div>
                </div>


                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-4 control-label">Lat : </label>

                  <div class="col-sm-8">
                     <label for="inputPassword3" class="control-label">{{$shops->lat}}</label>
                  </div>
                </div>


                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-4 control-label">Lng : </label>

                  <div class="col-sm-8">
                     <label for="inputPassword3" class="control-label">{{$shops->lng}}</label>
                  </div>
                </div>
              <!-- /.box-body -->
              <div class="box-footer">
                  <a class="btn btn-default" href="{{ URL::to('admin/shop/') }}">Back</a>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
      <!-- /.row -->
@endsection

@extends('admin.adminpen')
@section('content1')
<section class="content-header">
      <h1>
                {{ $page_title or "Page Title" }}
                <small>{{ $page_description or null }}</small>
            </h1>
      <ol class="breadcrumb">
        <li><a href="{{ URL::to('/admin/shop/create')}}"><i class="fa fa-dashboard"></i> New</a></li>
        <li class="active">Here</li>
      </ol>
    </section>
@endsection
@section('content')
    <div class="row">
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Quick Example</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
              <!-- if there are creation errors, they will show here -->
{{ Html::ul($errors->all()) }}

{{ Form::open(array('url' => 'admin/shop', 'role' => 'form')) }}
              <div class="box-body">

<div class="form-group">
                <label>Country</label>
                <select name="country_id" id="sel_depart" class="form-control select2" style="width: 100%;">
                  <option  disabled="true" selected="true">Country Name</option>
              @foreach($cou as $cous)
                  <option value="{{$cous->id}}">{{$cous->name}}</option>
                    @endforeach
                </select>
              </div>





               <div class="form-group" >
                <label>City</label>
                <select name="city_id" class="form-control select2" style="width: 100%;" id="sel_user">
                <option disabled="true" selected="true">City</option>
                  </select>
              </div>


              <div class="form-group" >
                <label>Township</label>
                <select name="township_id" class="form-control select2" style="width: 100%;" id="township">
                <option disabled="true" selected="true">Township</option>
                  </select>
              </div>


              <div class="form-group">
      <label>Shop</label>

 <input type="text" class="form-control" placeholder="Enter ..." name="shop_name" />


    </div>

    <div class="form-group">
      <label>Phone</label>

 <input type="text" class="form-control" placeholder="Enter ..." name="phone" />


    </div>


    <div class="form-group">
      <label>Address</label>

 <input type="text" class="form-control" placeholder="Enter ..." name="address" />


    </div>


    <div class="form-group">
      <label>Lat</label>

 <input type="text" class="form-control" placeholder="Enter ..." name="lat" />


    </div>

     <div class="form-group">
      <label>Lng</label>

 <input type="text" class="form-control" placeholder="Enter ..." name="lng" />


    </div>


              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                {{ Form::submit('Create the Township!', array('class' => 'btn btn-primary')) }}
                 <a class="btn btn-default" style="color: black;"  href="{{ route('shop.index') }}">Back</a>
              </div>
              {{ Form::close() }}
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
@endsection
@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){

        $("#sel_depart").change(function(){
            var cat_id=$(this).val();
            var op=" ";

            $.ajax({
                type:'get',
                url:'{!!URL::to('findProductName')!!}',
                data:{'id':cat_id},
                success:function(data){
op+='<option value="0" selected disabled>City Name</option>';
                     $("#sel_user").empty();
                for( var i = 0; i<data.length; i++){
                 op+='<option value="'+data[i].id+'">'+data[i].name+'</option>';
                }
                $("#sel_user").append(op);
                },
                error:function(){
                }
            });
        });
    });
</script>


<script type="text/javascript">
    $(document).ready(function(){

        $("#sel_user").change(function(){
            var cat_id=$(this).val();
            var op=" ";

            $.ajax({
                type:'get',
                url:'{!!URL::to('findProductName1')!!}',
                data:{'id':cat_id},
                success:function(data){

op+='<option value="0" selected disabled>Township Name</option>';
                     $("#township").empty();
                for( var i = 0; i<data.length; i++){
                 op+='<option value="'+data[i].id+'">'+data[i].township_name+'</option>';
                }
                $("#township").append(op);
                },
                error:function(){

                }
            });
        });
    });
</script>

<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
@endsection
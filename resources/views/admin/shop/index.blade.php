@extends('admin.adminpen')
@section('content1')
<section class="content-header">
      <h1>
                {{ $page_title or "Page Title" }}
                <small>{{ $page_description or null }}</small>
            </h1>
      <ol class="breadcrumb">
        <li><a href="{{ URL::to('/admin/shop/create')}}"><i class="fa fa-dashboard"></i> New</a></li>
        <li class="active">Here</li>
      </ol>
      @if(Session::has('message'))
<div class="alert {{ Session::get('alert-class', 'alert-info alert-dismissible') }}">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <h4><i class="icon fa fa-check"></i> Alert!</h4>
                {{ Session::get('message') }}</div>
@endif
    </section>
@endsection
@section('content')
    <div class="box">
            <div class="box-header">
              <h3 class="box-title">Shop Table</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-striped">
                <thead>
                <tr>
                	<th>No</th>
                  <th>Country</th>
                  <th>City</th>
                   <th>Township</th>
                   <th>Shop</th>
                  <th>Process</th>

                </tr>
                </thead>
                <tbody>
               @foreach($shops as $key => $value)
        <tr>
        	<td></td>
            <td>{{ $value->country ? $value->country->name : ''}}</td>
            <td>{{ $value->city ? $value->city->name : ''}}</td>
            <td>{{ $value->township ? $value->township->township_name : ''}}</td>
            <td>{{ $value->shop_name }}</td>
            <!-- we will also add show, edit, and delete buttons -->
            <td>
              <table>
                <tr>
                  <td>

            <a class="btn btn-success" href="{{ URL::to('admin/shop/' . $value->id) }}">Show</a>
              </td>
              <!-- padding: top right bottom left;-->
                <td style="padding:0 5px 0 5px;">

                <a class="btn btn-primary" href="{{ URL::to('admin/shop/' . $value->id .'/edit') }}">Edit</a>
                </td>
                <td>

                <!-- we will add this later since its a little more complicated than the other two buttons -->
                {{ Form::open(array('url' => 'admin/shop/' . $value->id)) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::submit('Delete', array('class' => 'btn btn-warning')) }}
                {{ Form::close() }}
                </td>
                </tr>
</table>
            </td>

        </tr>
    @endforeach
                </tbody>
                <tfoot>
                <tr>
                  <th>No</th>
                  <th>Country</th>
                  <th>City</th>
                  <th>Township</th>
                  <th>Shop</th>
                  <th>Process</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
@endsection
@section('scripts')
<script>
  $(function () {
    $('#example1').DataTable()
    /*for number in table start*/
    var t = $('#example2').DataTable( {
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0,

        } ],
        "order": [[ 1, 'asc' ]],
/*for number in table end*/
/* for export button start*/
       dom: 'Bfrtip',
        buttons: [
            //'copy', 'csv', 'excel', 'pdf', 'print'
            {
                extend: 'copyHtml5',
                title: 'Data export copy',
                exportOptions: {
                    columns: [ 0, ':visible' ]
                }
            },
            {
                extend: 'csvHtml5',
                title: 'Data export csv',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'excelHtml5',
                title: 'Data export excel',
                exportOptions: {
                    columns: [ 1 ]
                }
            },
            {
                extend: 'pdfHtml5',
                title: 'Data export pdf',
                exportOptions: {
                    columns: [ 0, 1 ]
                }
            },
            {
                extend: 'print',
                title: 'Data export print'
            },
            'colvis'
        ],
/*for export button end*/
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : true
    });
    /*for number in table start*/
    t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
/*for number in table end*/
  });
</script>
@endsection
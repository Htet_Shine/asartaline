@extends('admin.adminpen')

@section('content')
    <!-- SELECT2 EXAMPLE -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Select2</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
             <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                  <label>Text</label>
                  <input type="text" class="form-control" placeholder="Enter ...">
                </div>
              <!-- /.form-group -->
            <div class="form-group">
                <label>Date:</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker">
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form-group -->
            </div>
            <!-- /.col -->
          </div>

          <div class="row">
            <div class="col-md-2">
              <div class="form-group">
                <label>အလ်ား</label>
                <input type="text" class="form-control" placeholder="Enter ...">
              </div>

            </div>
            <!-- /.col -->
            <div class="col-md-2">
              <div class="form-group">
                <label>အနံ</label>
<input type="text" class="form-control" placeholder="Enter ...">
              </div>
            </div>

            <div class="col-md-2">
              <div class="form-group">
                <label>အျမင့္</label>
                <input type="text" class="form-control" placeholder="Enter ...">
              </div>
            </div>

            <div class="col-md-2">
              <div class="form-group">
                <label>စုစုေပါင္း</label>
<input type="text" class="form-control" placeholder="Enter ...">
              </div>
            </div>
            <!-- /.col -->
          </div>


            <div class="row">
            <div class="col-md-2">
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Enter ...">
              </div>
            </div>
            <!-- /.col -->
            <div class="col-md-2">
              <div class="form-group">
<input type="text" class="form-control" placeholder="Enter ...">
              </div>
            </div>

            <div class="col-md-2">
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Enter ...">
              </div>
            </div>

            <div class="col-md-2">
              <div class="form-group">
<input type="text" class="form-control" placeholder="Enter ...">
              </div>
            </div>
            <!-- /.col -->
          </div>

            <div class="row">
            <div class="col-md-2">
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Enter ...">
              </div>
            </div>
            <!-- /.col -->
            <div class="col-md-2">
              <div class="form-group">
<input type="text" class="form-control" placeholder="Enter ...">
              </div>
            </div>

            <div class="col-md-2">
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Enter ...">
              </div>
            </div>

            <div class="col-md-2">
              <div class="form-group">
<input type="text" class="form-control" placeholder="Enter ...">
              </div>
            </div>
            <!-- /.col -->
          </div>

            <div class="row">
            <div class="col-md-2">
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Enter ...">
              </div>
            </div>
            <!-- /.col -->
            <div class="col-md-2">
              <div class="form-group">
<input type="text" class="form-control" placeholder="Enter ...">
              </div>
            </div>

            <div class="col-md-2">
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Enter ...">
              </div>
            </div>

            <div class="col-md-2">
              <div class="form-group">
<input type="text" class="form-control" placeholder="Enter ...">
              </div>
            </div>
            <!-- /.col -->
          </div>

            <div class="row">
            <div class="col-md-2">
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Enter ...">
              </div>
            </div>
            <!-- /.col -->
            <div class="col-md-2">
              <div class="form-group">
<input type="text" class="form-control" placeholder="Enter ...">
              </div>
            </div>

            <div class="col-md-2">
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Enter ...">
              </div>
            </div>

            <div class="col-md-2">
              <div class="form-group">
<input type="text" class="form-control" placeholder="Enter ...">
              </div>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <!-- Visit <a href="https://select2.github.io/">Select2 documentation</a> for more examples and information about
          the plugin. -->
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">

              </div>
            </div>
            <!-- /.col -->
            <div class="col-md-2">
              <div class="form-group">
                အားလံုးစုစုေပါင္း
              </div>
            </div>

            <div class="col-md-2">
              <div class="form-group">
<input type="text" class="form-control" placeholder="Enter ...">

              </div>
            </div>

                <button type="submit" class="btn btn-primary">Submit</button>

            <!-- /.col -->
          </div>
        </div>
      </div>
      <!-- /.box -->
@endsection
@section('scripts')
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
@endsection
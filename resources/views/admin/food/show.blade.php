
@extends('admin.adminpen')
@section('content1')
<section class="content-header">
      <h1>
                {{ $page_title or "Page Title" }}
                <small>{{ $page_description or null }}</small>
            </h1>
      <ol class="breadcrumb">
        <li><a href="{{ URL::to('/admin/food/create')}}"><i class="fa fa-dashboard"></i> New</a></li>
        <li class="active">Here</li>
      </ol>
    </section>
@endsection
@section('content')
    <div class="row">
        <!-- right column -->
        <div class="col-md-6">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Food Detail</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal">
              <div class="box-body">

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-4 control-label">Taste Type  : </label>

                  <div class="col-sm-8">
                     <label for="inputPassword3" class="control-label">{{$foods->tastetype_name}}</label>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-4 control-label">Shop : </label>

                  <div class="col-sm-8">
                     <label for="inputPassword3" class="control-label">{{$foods->shop_name}}</label>
                  </div>
                </div>


                    <div class="form-group">
                  <label for="inputPassword3" class="col-sm-4 control-label">Food Name : </label>

                  <div class="col-sm-8">
                     <label for="inputPassword3" class="control-label">{{$foods->name}}</label>
                  </div>
                </div>


                 <div class="form-group">
                  <label for="inputPassword3" class="col-sm-4 control-label">Description  : </label>

                  <div class="col-sm-8">
                     <label for="inputPassword3" class="control-label">{{$foods->description}}</label>
                  </div>
                </div>


                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-4 control-label">Price  : </label>

                  <div class="col-sm-8">
                     <label for="inputPassword3" class="control-label">{{$foods->price}}</label>
                  </div>
                </div>


                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-4 control-label">Image  : </label>

                  <div class="col-sm-8">
                     <label for="inputPassword3" class="control-label"><img class="img-responsive img-thumbnail" src="/images/food/{{ $foods->image }}" alt="Banner" width="100" height="100"></label>
                  </div>
                </div>


                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-4 control-label">Suited  : </label>

                  <div class="col-sm-8">
                     <label for="inputPassword3" class="control-label">{{$foods->suited}}</label>
                  </div>
                </div>




              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                  <a class="btn btn-default" href="{{ URL::to('admin/food/') }}">Back</a>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
      <!-- /.row -->
@endsection
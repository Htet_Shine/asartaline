@extends('admin.adminpen')
@section('content1')
<section class="content-header">
      <h1>
                {{ $page_title or "Page Title" }}
                <small>{{ $page_description or null }}</small>
            </h1>
      <ol class="breadcrumb">
        <li><a href="{{ URL::to('/admin/food/create')}}"><i class="fa fa-dashboard"></i> New</a></li>
        <li class="active">Here</li>
      </ol>
    </section>
@endsection
@section('content')
    <div class="row">
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Quick Example</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
              <!-- if there are creation errors, they will show here -->
{{ Html::ul($errors->all()) }}

{!! Form::model($foods, ['method' => 'PUT', 'route' => ['food.update', $foods->id], 'files' => true]) !!}
              <div class="box-body">
                <div class="form-group">
                <label>TasteType</label>
                  <select name="tastetype_id" class="form-control select2" style="width: 100%;">
<option>Choose</option>
                    @foreach($tastes as $taste)
                    <option value="{{ $taste-> id }}" {{ ($foods->tastetype_id == $taste-> id ? "selected":"") }}>{{ $taste->name }}</option>
                    @endforeach
                  </select>


              </div>

                <div class="form-group">
                    <label>Shop</label>
                 <select name="shop_id" required="required" class="form-control select2" style="width: 100%;">
                    <option>Choose</option>

                    @foreach($shops as $shop)
                    <option value="{{ $shop-> id }}" {{ ($foods->shop_id == $shop-> id ? "selected":"") }}>{{ $shop-> shop_name }}</option>
                    @endforeach



                  </select>
                </div>

                <div class="form-group">
                    <label>Food Name</label>
                  <input type="text" class="form-control" name="name" value="{{$foods->name}}" />

                </div>


                <div class="form-group">
                    <label>Description</label>
                  <input type="text" class="form-control" name="description" value="{{$foods->description}}" />

                </div>


                <div class="form-group">
                    <label>Price</label>
                  <input type="text" class="form-control" name="price" value="{{$foods->price}}" />

                </div>




                 <div class="form-group">
      <label>Image</label>

 <input type="file" class="form-control" name="image" />
<img class="img-responsive img-thumbnail" src="/images/food/{{ $foods->image }}" alt="image" width="100" height="100">

    </div>


                <div class="form-group">
                    <label>Suited</label>
                  <input type="text" class="form-control" name="suited" value="{{$foods->suited}}" />

                </div>


              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                {{ Form::submit('Edit the Food!', array('class' => 'btn btn-primary')) }}
                <a class="btn btn-default" style="color: black;"  href="{{ route('food.index') }}">Back</a>
              </div>
              {{ Form::close() }}
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
@endsection
@section('scripts')
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
<script type="text/javascript">
    $(document).ready(function(){

        $("#sel_depart").change(function(){
            var cat_id=$(this).val();
            var op=" ";
            $.ajax({
                type:'get',
                url:'{!!URL::to('findProductName')!!}',
                data:{'id':cat_id},
                success:function(data){
op+='<option value="0" selected disabled>City Name</option>';
                     $("#sel_user").empty();
                for( var i = 0; i<data.length; i++){
                 op+='<option value="'+data[i].id+'">'+data[i].name+'</option>';
                }
                $("#sel_user").append(op);
                },
                error:function(){
                }
            });
        });
    });
</script>

@endsection
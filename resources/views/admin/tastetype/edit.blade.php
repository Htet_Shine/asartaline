@extends('admin.adminpen')
@section('content1')
<section class="content-header">
      <h1>
                {{ $page_title or "Page Title" }}
                <small>{{ $page_description or null }}</small>
            </h1>
      <ol class="breadcrumb">
        <li><a href="{{ URL::to('/admin/tastetype/create')}}"><i class="fa fa-dashboard"></i> New</a></li>
        <li class="active">Here</li>
      </ol>
    </section>
@endsection
@section('content')
    <div class="row">
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Quick Example</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
              <!-- if there are creation errors, they will show here -->
{{ Html::ul($errors->all()) }}

{{ Form::model($taste, array('route' => array('tastetype.update', $taste->id), 'method' => 'PUT')) }}
              <div class="box-body">
                <div class="form-group">
                  {{ Form::label('name', 'Name') }}
        {{ Form::text('name', null, array('class' => 'form-control')) }}


                </div>


              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                {{ Form::submit('Edit the TasteType!', array('class' => 'btn btn-primary')) }}
                <a class="btn btn-default" style="color: black;"  href="{{ route('tastetype.index') }}">Back</a>
              </div>
              {{ Form::close() }}
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
@endsection

@extends('admin.adminpen')
@section('content1')
<section class="content-header">
      <h1>
                {{ $page_title or "Page Title" }}
                <small>{{ $page_description or null }}</small>
            </h1>
      <ol class="breadcrumb">
        <li><a href="{{ URL::to('/admin/country/create')}}"><i class="fa fa-dashboard"></i> New</a></li>
        <li class="active">Here</li>
      </ol>
    </section>
@endsection
@section('content')
    <div class="row">
        <!-- right column -->
        <div class="col-md-6">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Country Detail</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal">
              <div class="box-body">

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-3 control-label">Name : </label>

                  <div class="col-sm-9">
                     <label for="inputPassword3" class="control-label">{{$cou->name}}</label>
                  </div>
                </div>


                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-3 control-label">Code : </label>

                  <div class="col-sm-9">
                     <label for="inputPassword3" class="control-label">{{$cou->code}}</label>
                  </div>
                </div>

              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                  <a class="btn btn-default" href="{{ URL::to('admin/country/') }}">Back</a>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
      <!-- /.row -->
@endsection